export class Employee {
    id: string;
    fullName: string;
    empCode: string;
    position: string;
    mobile: string;
    qf: string;
    qs: string;
    qt: string;
    q4: string;
}
